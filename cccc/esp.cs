﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using BrokeProtocol.Entities;
using BrokeProtocol.Properties;
using BrokeProtocol.Required;
using UnityEngine.Rendering;
using BrokeProtocol.Utility;
using BrokeProtocol.Managers;

namespace brokeprotocolHACK
{
    class esp : MonoBehaviour
    {
        
        public float FLT_MAX { get; private set; } = 3.402823466e+38F;
        public float FLT_MIN { get; private set; } = 1.175494351e-38F;

       
        public static bool chams = false;
        public static bool D_box = true;
        public static bool box3d = false;
        public static bool player_info = false;
        public static bool transport_esp = false;
        public static bool _skeleton = false;


        public static int cases = 0;
        public static float r = 1.00f;
        public static float g = 0.00f;
        public static float b = 1.00f;

        public void Start()
        {

        }

        public void Update()
        {

        }

        public void OnGUI()
        {
            if (Event.current.type != EventType.Repaint)
            {
                return;
            }


            if (Camera.current == null && CacheEntities.localPlayer == null)
                return;

            switch (cases)
            {
                case 0: { r -= 0.0150f; if (r <= 0) cases += 1; break; }
                case 1: { g += 0.0150f; b -= 0.0150f; if (g >= 1) cases += 1; break; }
                case 2: { r += 0.0150f; if (r >= 1) cases += 1; break; }
                case 3: { b += 0.0150f; g -= 0.0150f; if (b >= 1) cases = 0; break; }
                default: { r = 1.00f; g = 0.00f; b = 1.00f; break; }
            }

            boudnsesp();

            if (_skeleton)
            {
                skeleton();
            }

            if (player_info)
            {
                displayname_health();
                ShowJobName_dist();
                HeldItems_money();
            }

            if (chams)
            {
                chams_();
                viewmodelchams();
            }


            if (transport_esp)
            {
                Transport();
               //bullets();
            }

            ESPUtils.DrawCircle(Color.white, new Vector2(Screen.width / 2, Screen.height / 2), aimbot.aimfov);

            muzzle_point();
            target_line();

        }

        private static void bullets()
        {

            if (CacheEntities.entities.Length > 0)
            {
                foreach (ShEntity entity in CacheEntities.entities)
                {
                        var distance =
                            Mathf.Round(Vector3.Distance(CacheEntities.localPlayer.transform.position, entity.transform.position));

                        var centre = entity.transform.position;
                        var w_s_c = CacheEntities.worldCamera.WorldToScreenPoint(centre);
                        w_s_c.y = Screen.height - (w_s_c.y + 1f);

                        if (ESPUtils.IsOnScreen(w_s_c))
                        {
                            //if (entity.name == "Safe")
                            ESPUtils.DrawString(w_s_c, entity.name, Color.red, true, 12, FontStyle.Bold, 1);
                        }
                    }
            }
        }

        private static void target_line()
        {
            if (CacheEntities.nearestTarget.characterType == CharacterType.Humanoid && !CacheEntities.nearestTarget.IsDead &&
                CacheEntities.nearestTarget != CacheEntities.localPlayer && CacheEntities.nearestTarget == CacheEntities.nearestTarget.isHuman)
            {
                var head = CacheEntities.nearestTarget.clPlayer.headBone.transform.position + new Vector3(0, 1, 0);
                var muzzle = CacheEntities.get_activeWeapon.curMuzzle.transform.position;
                var muzzle_forward = muzzle + CacheEntities.get_activeWeapon.curMuzzle.transform.forward * 2f;
                var bone_head = CacheEntities.worldCamera.WorldToScreenPoint(head);
                var bone_muzzle = CacheEntities.worldCamera.WorldToScreenPoint(muzzle);
                var bone_muzzle_forward = CacheEntities.worldCamera.WorldToScreenPoint(muzzle_forward);

                bone_head.y = Screen.height - (bone_head.y + 1f);
                bone_muzzle.y = Screen.height - (bone_muzzle.y + 1f);
                bone_muzzle_forward.y = Screen.height - (bone_muzzle_forward.y + 1f);



                if (ESPUtils.IsOnScreen(bone_head) && ESPUtils.IsOnScreen(bone_muzzle) && ESPUtils.IsOnScreen(bone_muzzle_forward))
                {
                    if (CacheEntities.nearestTarget != null && !CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Zoom))
                    {
                        ESPUtils.DrawLine(bone_muzzle_forward, bone_head, Color.red, 1.5f);
                    }
                    else
                    {
                        ESPUtils.DrawLine(new Vector2(Screen.width / 2, Screen.height / 2), bone_head, Color.red, 1.5f);
                    }
                }
            }
        }

        private static void muzzle_point()
        {

            var muzzle = CacheEntities.get_activeWeapon.curMuzzle.transform.position;
            var muzzle_forward = muzzle + CacheEntities.get_activeWeapon.curMuzzle.transform.forward * 2f;
            var bone_muzzle = CacheEntities.worldCamera.WorldToScreenPoint(muzzle);
            var bone_muzzle_forward = CacheEntities.worldCamera.WorldToScreenPoint(muzzle_forward);

            bone_muzzle.y = Screen.height - (bone_muzzle.y + 1f);
            bone_muzzle_forward.y = Screen.height - (bone_muzzle_forward.y + 1f);



            

            if (ESPUtils.IsOnScreen(bone_muzzle) && ESPUtils.IsOnScreen(bone_muzzle_forward))
            {
                if (!CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Zoom))
                {
                    ESPUtils.DrawLine(bone_muzzle, bone_muzzle_forward, Color.white, 1.5f);
                }
                else
                {
                    
                }
            }
        }

        private static void skeleton()
        {

            if (CacheEntities.players.Length > 0)
            {
                foreach (ShPlayer player in CacheEntities.players)
                {
                    if (player != null && player.characterType == CharacterType.Humanoid && !player.IsDead &&
                        player != CacheEntities.localPlayer && player == player.isHuman)
                    {
                        var centre = player.transform.position;
                        var head = player.clPlayer.headBone.transform.position + new Vector3(0, 1, 0);
                        var upper_chest = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.neck);
                        var chest = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.spine);
                        var r_upper_arm = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.r_arm);
                        var l_upper_arm = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.l_arm);
                        var r_fore_arm = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.r_fore_arm);
                        var l_fore_arm = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.l_fore_arm);
                        var r_hand = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.r_hand);
                        var l_hand = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.l_hand);
                        var pelvis = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.hips);
                        var r_hip = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.r_up_leg);
                        var l_hip = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.l_up_leg);
                        var r_leg = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.r_leg);
                        var l_leg = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.l_leg);
                        var r_foot = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.r_foot);
                        var l_foot = CacheEntities.GetBonePosition(player, CacheEntities.bone_list.l_foot);

                        var transform_pos = CacheEntities.worldCamera.WorldToScreenPoint(centre);
                        var bone_head = CacheEntities.worldCamera.WorldToScreenPoint(head);
                        var bone_upper_chest = CacheEntities.worldCamera.WorldToScreenPoint(upper_chest);
                        var bone_r_upper_arm = CacheEntities.worldCamera.WorldToScreenPoint(r_upper_arm);
                        var bone_l_upper_arm = CacheEntities.worldCamera.WorldToScreenPoint(l_upper_arm);
                        var bone_r_fore_arm = CacheEntities.worldCamera.WorldToScreenPoint(r_fore_arm);
                        var bone_l_fore_arm = CacheEntities.worldCamera.WorldToScreenPoint(l_fore_arm);
                        var bone_r_hand = CacheEntities.worldCamera.WorldToScreenPoint(r_hand);
                        var bone_l_hand = CacheEntities.worldCamera.WorldToScreenPoint(l_hand);
                        var bone_spine = CacheEntities.worldCamera.WorldToScreenPoint(chest);
                        var bone_pelvis = CacheEntities.worldCamera.WorldToScreenPoint(pelvis);
                        var bone_r_hip = CacheEntities.worldCamera.WorldToScreenPoint(r_hip);
                        var bone_l_hip = CacheEntities.worldCamera.WorldToScreenPoint(l_hip);
                        var bone_r_leg = CacheEntities.worldCamera.WorldToScreenPoint(r_leg);
                        var bone_l_leg = CacheEntities.worldCamera.WorldToScreenPoint(l_leg);
                        var bone_r_foot = CacheEntities.worldCamera.WorldToScreenPoint(r_foot);
                        var bone_l_foot = CacheEntities.worldCamera.WorldToScreenPoint(l_foot);

                        transform_pos.y = Screen.height - (transform_pos.y + 1f);

                        bone_head.y = Screen.height - (bone_head.y + 1f);
                        bone_upper_chest.y = Screen.height - (bone_upper_chest.y + 1f);
                        bone_r_upper_arm.y = Screen.height - (bone_r_upper_arm.y + 1f);
                        bone_l_upper_arm.y = Screen.height - (bone_l_upper_arm.y + 1f);
                        bone_spine.y = Screen.height - (bone_spine.y + 1f);
                        bone_r_fore_arm.y = Screen.height - (bone_r_fore_arm.y + 1f);
                        bone_l_fore_arm.y = Screen.height - (bone_l_fore_arm.y + 1f);
                        bone_r_hand.y = Screen.height - (bone_r_hand.y + 1f);
                        bone_l_hand.y = Screen.height - (bone_l_hand.y + 1f);
                        bone_pelvis.y = Screen.height - (bone_pelvis.y + 1f);
                        bone_r_hip.y = Screen.height - (bone_r_hip.y + 1f);
                        bone_l_hip.y = Screen.height - (bone_l_hip.y + 1f);
                        bone_r_leg.y = Screen.height - (bone_r_leg.y + 1f);
                        bone_l_leg.y = Screen.height - (bone_l_leg.y + 1f);
                        bone_r_foot.y = Screen.height - (bone_r_foot.y + 1f);
                        bone_l_foot.y = Screen.height - (bone_l_foot.y + 1f);

                        if (ESPUtils.IsOnScreen(bone_spine) && ESPUtils.IsOnScreen(bone_head) && ESPUtils.IsOnScreen(transform_pos) && ESPUtils.IsOnScreen(bone_pelvis)
                            && ESPUtils.IsOnScreen(bone_upper_chest) && ESPUtils.IsOnScreen(bone_r_upper_arm) && ESPUtils.IsOnScreen(bone_l_upper_arm) && ESPUtils.IsOnScreen(bone_r_fore_arm) &&
                            ESPUtils.IsOnScreen(bone_l_fore_arm) && ESPUtils.IsOnScreen(bone_r_hand) && ESPUtils.IsOnScreen(bone_l_hand) && ESPUtils.IsOnScreen(bone_r_hip) && ESPUtils.IsOnScreen(bone_l_hip)
                            && ESPUtils.IsOnScreen(bone_r_foot) && ESPUtils.IsOnScreen(bone_l_foot) && ESPUtils.IsOnScreen(bone_r_leg) && ESPUtils.IsOnScreen(bone_l_leg))
                        {
                            ESPUtils.DrawLine(bone_head, bone_spine, Color.white, 1f);
                            ESPUtils.DrawLine(bone_upper_chest, bone_r_upper_arm, Color.white, 1f);
                            ESPUtils.DrawLine(bone_upper_chest, bone_l_upper_arm, Color.white, 1f);
                            ESPUtils.DrawLine(bone_r_upper_arm, bone_r_fore_arm, Color.white, 1f);
                            ESPUtils.DrawLine(bone_l_upper_arm, bone_l_fore_arm, Color.white, 1f);
                            ESPUtils.DrawLine(bone_r_fore_arm, bone_r_hand, Color.white, 1f);
                            ESPUtils.DrawLine(bone_l_fore_arm, bone_l_hand, Color.white, 1f);
                            ESPUtils.DrawLine(bone_spine, bone_pelvis, Color.white, 1f);
                            ESPUtils.DrawLine(bone_pelvis, bone_r_hip, Color.white, 1f);
                            ESPUtils.DrawLine(bone_pelvis, bone_l_hip, Color.white, 1f);
                            ESPUtils.DrawLine(bone_r_hip, bone_r_leg, Color.white, 1f);
                            ESPUtils.DrawLine(bone_l_hip, bone_l_leg, Color.white, 1f);
                            ESPUtils.DrawLine(bone_r_leg, bone_r_foot, Color.white, 1f);
                            ESPUtils.DrawLine(bone_l_leg, bone_l_foot, Color.white, 1f);

                        }
                    }
                }
            }
        }

        private static void Transport()
        {

            if (CacheEntities.Transports.Length > 0)
            {
                foreach (ShTransport transport in CacheEntities.Transports)
                {
                    if (transport != null && !CacheEntities.localPlayer.IsDead)
                    {
                        var distance = Mathf.Round(Vector3.Distance(CacheEntities.localPlayer.transform.position,
                            transport.transform.position));
                        var transportName = transport.name;
                        var tranporthealth = transport.health;
                        var infoText = $"{transportName} distance[{distance}] HP:{tranporthealth}".Trim();

                        var centre = transport.transform.position;

                        var w_s_c = CacheEntities.worldCamera.WorldToScreenPoint(centre);
                        w_s_c.y = Screen.height - (w_s_c.y + 1f);


                        if (ESPUtils.IsOnScreen(w_s_c))
                        {
                            ESPUtils.DrawString(w_s_c, infoText, Color.white, true, 12, FontStyle.Bold, 1);
                        }
                    }
                }
            }
        }

        private static void ShowJobName_dist()
        {

            if (CacheEntities.players.Length > 0)
            {
                foreach (ShPlayer player in CacheEntities.players)
                {
                    if (player != null && player.characterType == CharacterType.Humanoid && !player.IsDead &&
                        player != CacheEntities.localPlayer && player == player.isHuman)
                    {
                        var distance =
                            Mathf.Round(Vector3.Distance(CacheEntities.localPlayer.transform.position, player.transform.position));
                        var JobName = player.clPlayer.job.jobName;
                        var idplayer = player.ID;
                        var infoText = $"Job:{JobName} distance[{distance}] ID:{idplayer}".Trim();

                        var centre = player.transform.position;
                        var head = centre + new Vector3(0f, 3f, 0f);
                        var legs = centre - new Vector3(0f, 1f, 0f);

                        var w_s_h = CacheEntities.worldCamera.WorldToScreenPoint(head);
                        var l_s_l = CacheEntities.worldCamera.WorldToScreenPoint(legs);
                        l_s_l.y = Screen.height - (l_s_l.y + 1f);

                        if (ESPUtils.IsOnScreen(l_s_l))
                        {
                            ESPUtils.DrawString(l_s_l, infoText, Color.white, true, 12, FontStyle.Bold, 1);
                        }
                    }
                }
            }
        }

        private static void HeldItems_money()
        {

            if (CacheEntities.players.Length > 0)
            {
                foreach (ShPlayer player in CacheEntities.players)
                {
                    if (player != null && player.characterType == CharacterType.Humanoid && !player.IsDead &&
                        player != CacheEntities.localPlayer && player == player.isHuman)
                    {
                        var itemname = player.curEquipable.itemName;
                        var infoText = $"{itemname}".Trim();

                        var centre = player.transform.position;
                        var head = centre + new Vector3(0f, 3f, 0f);
                        var legs = centre - new Vector3(0f, 3f, 0f);

                        var w_s_h = CacheEntities.worldCamera.WorldToScreenPoint(head);
                        var l_s_l = CacheEntities.worldCamera.WorldToScreenPoint(legs);
                        l_s_l.y = Screen.height - (l_s_l.y + 1f);

                        if (ESPUtils.IsOnScreen(l_s_l))
                        {

                            ESPUtils.DrawString(l_s_l, infoText, Color.white, true, 12, FontStyle.Bold, 1);

                        }
                    }
                }
            }
        }
        
        private static void displayname_health()
        {

            if (CacheEntities.players.Length > 0)
            {
                foreach (ShPlayer player in CacheEntities.players)
                {
                    if (player != null && player.characterType == CharacterType.Humanoid && !player.IsDead &&
                        player != CacheEntities.localPlayer && player == player.isHuman)
                    {
                        var displayNickname = player.displayName;
                        var health = player.health;
                        var infoText = $"{displayNickname} HP:{health}".Trim();

                        var centre = player.transform.position;
                        var head = centre + new Vector3(0f, 8f, 0f);
                        var legs = centre - new Vector3(0f, 0.5f, 0f);

                        var w_s_h = CacheEntities.worldCamera.WorldToScreenPoint(head);
                        var l_s_l = CacheEntities.worldCamera.WorldToScreenPoint(legs);
                        w_s_h.y = Screen.height - (w_s_h.y + 1f);

                        if (ESPUtils.IsOnScreen(w_s_h))
                        {
                            ESPUtils.DrawString(w_s_h, infoText, Color.white, true, 12, FontStyle.Bold, 1);
                        }
                    }
                }
            }
        }

        private static void viewmodelchams()
        {



            foreach (var renderer in CacheEntities.get_activeWeapon.GetComponentsInChildren<Renderer>())
            {
                if (renderer == null)
                    continue;
                if (renderer.material == null)
                    continue;

                var bundle = main.Bundle.LoadAsset<Shader>("Force Field.shader");
                renderer.material.shader = bundle;
                renderer.material.SetColor("_Color", new Color(r, g, b, 3));

            }

        }

        private static void chams_()
        {

            foreach (ShPlayer player in CacheEntities.players)
            {
                if (player != null && player.characterType == CharacterType.Humanoid && !player.IsDead &&
                    player != CacheEntities.localPlayer && player == player.isHuman)
                {
                    foreach (var renderer in player.GetComponentsInChildren<Renderer>())
                    {
                        if (renderer == null)
                            continue;
                        if (renderer.material == null)
                            continue;

                        var bundle = main.Bundle.LoadAsset<Shader>("Force Field.shader");
                        renderer.material.shader = bundle;
                        renderer.material.SetColor("_Color", new Color(r, g, b, 3));

                        /* renderer.material = new Material(Shader.Find("Hidden/Internal-Colored"))
                         {
                             hideFlags = HideFlags.DontSaveInEditor | HideFlags.HideInHierarchy
                         };
                         renderer.material.SetInt("_Cull", 0);
                         renderer.material.SetInt("_ZWrite", 0);
                         renderer.material.SetInt("_ZTest", 8);
                         if (player.displayName == main.friend)
                         {
                             renderer.material.SetColor("_Color", Color.green);
                         }
                         else
                         {
                             renderer.material.SetColor("_Color", new Color(r, g, b, 1));
                         }*/
                    }
                }
            }
        }
        private static List<CacheEntities.bone_list> list_bone = new List<CacheEntities.bone_list>()
        {
            CacheEntities.bone_list.hips, CacheEntities.bone_list.l_up_leg, CacheEntities.bone_list.l_leg, CacheEntities.bone_list.l_foot, CacheEntities.bone_list.r_up_leg, CacheEntities.bone_list.r_leg,
            CacheEntities.bone_list.r_foot, CacheEntities.bone_list.spine, CacheEntities.bone_list.spine1, CacheEntities.bone_list.l_arm, CacheEntities.bone_list.l_fore_arm, CacheEntities.bone_list.l_hand,
            CacheEntities.bone_list.r_arm, CacheEntities.bone_list.r_fore_arm, CacheEntities.bone_list.r_hand, CacheEntities.bone_list.neck
        };
        
        private static void boudnsesp()
        {
            foreach (ShPlayer player in CacheEntities.players)
            {
                if (player != null && player.characterType == CharacterType.Humanoid && !player.IsDead &&
                    player != CacheEntities.localPlayer && player == player.isHuman)
                {
                    List<Transform> bones = player.clPlayer.skinnedMeshRenderer.bones.ToList();
                    if (bones.Count == 0)
                        return;
                    var mins = bones[0].position;
                    var maxs = mins;
                    bones.RemoveAt(0);
                    Vector3 position;
                    for (int i = 0; i < bones.Count; i++)
                    {
                        var bone = bones[i];
                        if (bone == null || !list_bone.Contains((CacheEntities.bone_list)i))
                            continue;
                        position = bone.position;
                        if (mins.x > position.x)
                            mins.x = position.x;
                        if (mins.y > position.y)
                            mins.y = position.y;
                        if (mins.z > position.z)
                            mins.z = position.z;

                        if (maxs.x < position.x)
                            maxs.x = position.x;
                        if (maxs.y < position.y)
                            maxs.y = position.y;
                        if (maxs.z < position.z)
                            maxs.z = position.z;
                    }

                    Vector3[] bounds = new Vector3[8]
                 {
                //Lower parts
                new Vector3(mins.x, mins.y, mins.z), //LB
                new Vector3(maxs.x, mins.y, mins.z), //RB
                new Vector3(mins.x, mins.y, maxs.z), //LF
                new Vector3(maxs.x, mins.y, maxs.z), //RF
                //Upper parts
                new Vector3(mins.x, maxs.y + 1.5f, mins.z), //LB
                new Vector3(maxs.x, maxs.y + 1.5f, mins.z), //RB
                new Vector3(mins.x, maxs.y + 1.5f, maxs.z), //LF
                new Vector3(maxs.x, maxs.y + 1.5f, maxs.z), //RF
                 };

                    Vector3[] w2s = new Vector3[8];
                    for (int i = 0; i < 8; i++)
                    {
                        w2s[i] = CacheEntities.worldCamera.WorldToScreenPoint(bounds[i]);
                        w2s[i].y = Screen.height - w2s[i].y;
                    }

                    mins = w2s[0];
                    maxs = mins;
                    for (int i = 1; i < 8; i++)
                    {
                        if (w2s[i].z < 0.01f)
                            continue;
                        if (mins.x > w2s[i].x)
                            mins.x = w2s[i].x;
                        if (mins.y > w2s[i].y)
                            mins.y = w2s[i].y;
                        if (maxs.x < w2s[i].x)
                            maxs.x = w2s[i].x;
                        if (maxs.y < w2s[i].y)
                            maxs.y = w2s[i].y;
                    }

                    if (box3d)
                    {
                        //DoLowerPart
                        if (w2s[0].z > 0.01f && w2s[1].z > 0.01f)
                            ESPUtils.DrawLine(w2s[0], w2s[1], Color.green, 1f);
                        if (w2s[0].z > 0.01f && w2s[2].z > 0.01f)
                            ESPUtils.DrawLine(w2s[0], w2s[2], Color.green, 1f);
                        if (w2s[1].z > 0.01f && w2s[3].z > 0.01f)
                            ESPUtils.DrawLine(w2s[1], w2s[3], Color.green, 1f);
                        if (w2s[2].z > 0.01f && w2s[3].z > 0.01f)
                            ESPUtils.DrawLine(w2s[2], w2s[3], Color.green, 1f);
                        //DoUpperPart
                        if (w2s[0 + 4].z > 0.01f && w2s[1 + 4].z > 0.01f)
                            ESPUtils.DrawLine(w2s[0 + 4], w2s[1 + 4], Color.green, 1f);
                        if (w2s[0 + 4].z > 0.01f && w2s[2 + 4].z > 0.01f)
                            ESPUtils.DrawLine(w2s[0 + 4], w2s[2 + 4], Color.green, 1f);
                        if (w2s[1 + 4].z > 0.01f && w2s[3 + 4].z > 0.01f)
                            ESPUtils.DrawLine(w2s[1 + 4], w2s[3 + 4], Color.green, 1f);
                        if (w2s[2 + 4].z > 0.01f && w2s[3 + 4].z > 0.01f)
                            ESPUtils.DrawLine(w2s[2 + 4], w2s[3 + 4], Color.green, 1f);
                        //Connect Lower and UpperPart
                        if (w2s[0].z > 0.01f && w2s[4].z > 0.01f)
                            ESPUtils.DrawLine(w2s[0], w2s[4], Color.green, 1f);
                        if (w2s[1].z > 0.01f && w2s[5].z > 0.01f)
                            ESPUtils.DrawLine(w2s[1], w2s[5], Color.green, 1f);
                        if (w2s[2].z > 0.01f && w2s[6].z > 0.01f)
                            ESPUtils.DrawLine(w2s[2], w2s[6], Color.green, 1f);
                        if (w2s[3].z > 0.01f && w2s[7].z > 0.01f)
                            ESPUtils.DrawLine(w2s[3], w2s[7], Color.green, 1f);
                    }

                    if (D_box)
                    {
                        Vector2[] Box2D = new Vector2[4]
                        {
                    new Vector3(mins.x, mins.y),
                    new Vector3(maxs.x, mins.y),
                    new Vector3(mins.x, maxs.y),
                    new Vector3(maxs.x, maxs.y)
                        };


                        ESPUtils.DrawLine(Box2D[0], Box2D[1], Color.green, 1f);
                        ESPUtils.DrawLine(Box2D[0], Box2D[2], Color.green, 1f);
                        ESPUtils.DrawLine(Box2D[2], Box2D[3], Color.green, 1f);
                        ESPUtils.DrawLine(Box2D[3], Box2D[1], Color.green, 1f);

                    }
                }
            }
        }
    }
}