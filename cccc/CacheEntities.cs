﻿using BrokeProtocol.Entities;
using BrokeProtocol.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrokeProtocol.Client.UI;
using BrokeProtocol.Utility.AI;
using UnityEngine;
using BrokeProtocol.Required;
using BrokeProtocol.Managers;
using System.Runtime.InteropServices;
using UnityEngine.InputSystem;
using System.IO;

namespace brokeprotocolHACK
{
    class CacheEntities : MonoBehaviour
    {
        [DllImport("User32.Dll")]
        public static extern int FindWindow(string lpClassName, string lpWindowName);



        protected bool eo_22 = true;

        public static ShTransport[] Transports;
        public static ShPlayer[] players;
        public static ShPlayer localPlayer;
        public static Camera worldCamera;
        public static ShGun shGun;
        public static ShEquipable get_activeWeapon;
        public static ShEntity[] entities;
        public static ShBullet[] bullets;
        public static ShBullet bullet1;
        public static ShBallistic[] shBallistics;
        public static ShBallistic ballistic;
        public static ShBallistic shBallistic;
        public static ShEntity VideoPlayer;
        public static ShEntity Safe;
        public static ShHitscan shHitscan;
        public static ShEntity droped_rocket;
        public static ShPlayer nearestTarget;
        public static Serialized[] serializeds;
        public static Serialized serialized;
        public static ShTransport nearestTransport;
        public static InventoryMenu inventoryMenu;
        public static ShDestroyable[] destroyables;
        public static ShDestroyable destroyable;
        public static ShThrown[] throwns;
        public static ShThrown thrown;
        public static WeaponSet GetWeaponSet;
        public static ShThrown thrownEntity;
        public static ShPhysical[] physicals;
        public static ShPhysical physical;
        public static ShThrown bulletThrown;
        public static ShPhysical rocketThrown;
        public static ShUsable shUsable;
        public static ShProjectile projectile;
        public static ShHitscan Hitscan;
        public static ShWeapon shWeapon;
        public static Vector2 get_mousePosition;
        public static Vector2 unity_mousePosition;
        public static SkinnedMeshRenderer skinnedMesh;
        public static SceneManager sceneManager;
        public static HackingMenu hackingMenu;

        protected void Update()
        {
           // if (eo_22)
            // {
            //     transform();
            // }

           

            players = FindObjectsOfType(typeof(ShPlayer)) as ShPlayer[];
            Transports = FindObjectsOfType(typeof(ShTransport)) as ShTransport[];
            bullets = FindObjectsOfType(typeof(ShBullet)) as ShBullet[];
            entities = FindObjectsOfType(typeof(ShEntity)) as ShEntity[];
            serializeds = FindObjectsOfType(typeof(Serialized)) as Serialized[];
            shBallistics = FindObjectsOfType(typeof(ShBallistic)) as ShBallistic[];
            destroyables = FindObjectsOfType(typeof(ShDestroyable)) as ShDestroyable[];
            throwns = FindObjectsOfType(typeof(ShThrown)) as ShThrown[];
            physicals = FindObjectsOfType(typeof(ShPhysical)) as ShPhysical[];

            foreach (ShPlayer player in players)
            {
                if (player == player.clPlayer.clManager.myPlayer)
                {
                    localPlayer = player;
                    continue;
                }
            }


            get_mousePosition = Mouse.current.position.ReadValue();
            unity_mousePosition = Event.current.mousePosition;

            clanTag();

            if (Camera.current == null && localPlayer == null)
                return;
            hackingMenu = localPlayer.manager.clManager.CurrentMenu as HackingMenu;
            sceneManager = SceneManager.Instance;
            skinnedMesh = localPlayer.clPlayer.skinnedMeshRenderer;

            foreach (ShEntity entity in entities)
            {
                if (entity.name == "VideoPlayer" || entity.name == "ComputerDisplayModern" || entity.name == "ComputerDisplayOffice2" || entity.name == "TV1" || entity.name == "TV2" || entity.name == "ComputerDisplayOffice1")
                {
                    VideoPlayer = entity;
                    continue;
                }
                if (entity.name == "Rocket")
                {
                    droped_rocket = entity;
                    continue;
                }
                if (entity.name == "Safe")
                {
                    Safe = entity;
                    continue;
                }
            }

            foreach (ShThrown _thrown in throwns)
            {
                if (_thrown != null)
                {
                    thrown = _thrown;
                    continue;
                }
            }

            foreach (ShDestroyable shDestroyable in destroyables)
            {
                if (shDestroyable != null)
                {
                    destroyable = shDestroyable;
                    continue;
                }
            }

            foreach (ShPhysical _physical in physicals)
            {
                if (_physical != null)
                {
                    var dist = Vector3.Distance(localPlayer.transform.position, _physical.transform.position);

                    if (_physical.name == "BulletThrown" && dist < 25f)
                    {
                        physical = _physical;
                        continue;
                    }
                    if ((_physical.name == "RocketGuidedThrown" || _physical.name == "RocketThrown") && dist < 30f)
                    {
                        rocketThrown = _physical;
                        continue;
                    }
                }
            }

            foreach (Serialized serialize in serializeds)
            {
                if (serialize != null)
                {
                    serialized = serialize;
                    continue;
                }
            }

            foreach (ShBallistic _ent in shBallistics)
            {
                if (_ent != null)
                {
                    ballistic = _ent;
                    continue;
                }
            }

            foreach (ShTransport transport in Transports)
            {
                if (transport != null)
                {
                    var distance =
                            Mathf.Round(Vector3.Distance(localPlayer.transform.position, transport.transform.position));
                    if (distance < 2)
                    {
                        if (transport.state == BrokeProtocol.Required.EntityState.ForSale || transport.state == BrokeProtocol.Required.EntityState.Locked)
                        {
                            nearestTransport = transport;
                            continue;
                        }
                    }
                }
            }

            foreach (ShBullet bullet in bullets)
            {
                if (bullet != null)
                {
                    bullet1 = bullet;
                    continue;
                }
            }

            get_activeWeapon = localPlayer.curEquipable;
            shBallistic = get_activeWeapon as ShBallistic;
            shGun = get_activeWeapon as ShGun;
            inventoryMenu = get_activeWeapon.clItem.inventoryMenu;
            shHitscan = get_activeWeapon as ShHitscan;
            shUsable = get_activeWeapon as ShUsable;
            projectile = shUsable as ShProjectile;
            shWeapon = localPlayer.Hands;

            worldCamera = MainCamera.Instance.worldCamera;


           // hooks.Init(); need fix hooks


        }

        public static Vector3 GetBonePosition(ShPlayer player, bone_list list)
        {
            return player.clPlayer.skinnedMeshRenderer.bones[(int)list].position;
        }

        public enum bone_list
        {
            hips = 0,
            l_up_leg = 1,
            l_leg = 2,
            l_foot = 3,
            r_up_leg = 4,
            r_leg = 5,
            r_foot = 6,
            spine = 7,
            spine1 = 8,
            l_arm = 9,
            l_fore_arm = 10,
            l_hand = 11,
            r_arm = 12,
            r_fore_arm = 13,
            r_hand = 14,
            neck = 15
        }


        public static void LoadShader(Shader shader, string name_file, string name_shader)
        {
            var filename = Path.Combine(Application.dataPath, name_file);
            if (!File.Exists(filename))
                return;

            var bundle = AssetBundle.LoadFromFile(filename);
            if (bundle == null)
                return;

            shader = bundle.LoadAsset<Shader>(name_shader);
        }

        protected void clanTag()
        {
            var a = localPlayer.manager.clManager.profileURL = "https://cdn.discordapp.com/attachments/880306774685655040/957246554048180234/unknown.png";
            PlayerPrefs.SetString("ProfileURL", a);
            PlayerPrefs.Save();
        }

        public static bool is_visible(GameObject obj, Vector3 start, Vector3 end)
        {
            RaycastHit hit;
            return Physics.Linecast(start, end, out hit, 8 | 18) && hit.collider && hit.collider.gameObject.transform.root.gameObject == obj.transform.root.gameObject;
        }

    }
}
