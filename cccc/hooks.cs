﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
namespace brokeprotocolHACK
{
    public static class hooks
    {
        public static HookManager SAimbotHook;
        public static HookManager onDamage;
        private static bool is_hooked = true;


        public static void Init()
        {
            MethodInfo orig = null;
            MethodInfo repl = null;
            if (SAimbotHook != null)
                SAimbotHook.Unhook();

            orig = GameModule.FindMethod("BrokeProtocol.Entities.ShHitscan", "FireVector");
            repl = typeof(aimbot).GetMethod("hk_FireVector", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);


            SAimbotHook = new HookManager(orig, repl);
           // SAimbotHook.Hook();
        }
    }
}
