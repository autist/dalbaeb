﻿using System;
using BrokeProtocol.Entities;
using System.Runtime.InteropServices;
using BrokeProtocol.Required;
using ENet;
using BrokeProtocol.Utility.Networking;
using BrokeProtocol.Utility;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using BrokeProtocol.Client.UI;
using BrokeProtocol.Managers;
using BrokeProtocol.GameSource.Types;
using BrokeProtocol.API;
using System.Collections;

namespace brokeprotocolHACK
{
    class main : MonoBehaviour
    {

        [DllImport("User32.dll")]
        public static extern bool GetAsyncKeyState(int ArrowKeys);

        private int mainWID;
        private Rect mainWRect = new Rect(5f, 5f, 500f, 350f);
        private bool drawMenu = true;
        private bool testzalup = false;
        protected static bool fast_move = false;
        protected static bool _norecoil = false;
        protected static bool WantedBypass = false;
        protected static bool chatspammer = false;
        protected static bool customFov = false;
        public static int CameraFov = 75;
        protected static bool flyhack = false;
        public bool message = false;
        protected static bool debugCamera = false;
        public bool camera_keyDown = false;
        protected static bool fast_bullets = false;
        protected static bool skyBox = false;
        protected static bool s_rainbow = false;
        protected static bool s_night = false;
        protected static int skyList = 0;
        private System.String textspam;
        private System.String discord_link;
        public static System.String friend;
        public static ShGun[] Guns;
        public float nextFire;
        public UnityEngine.EventType eventType = UnityEngine.Event.current.type;
        public static AssetBundle Bundle;
        private static int ent_id;

        public void init()
        {
            if (!Bundle)
                Bundle = AssetBundle.LoadFromFile(@"");
        }

        public void OnGUI()
        {

           init();

            GUI.Label(new Rect(500f, 30f, 300f, 100f), "BMB - BLACK MONKEY BALLS");


            GUI.Label(new Rect(100f, 30f, 300f, 60f), "aimbot fov: " + aimbot.aimfov);
            GUI.Label(new Rect(100f, 45f, 300f, 60f), "aimbot distance: " + aimbot.MaximumDistance);
            GUI.Label(new Rect(100f, 60f, 300f, 60f), "camera fov: " + MainCamera.Instance.defaultFOV);


            /*GUI.Label(new Rect(500f, 75f, 300f, 300f), "bone list: " 
                
                
                
                
                + CacheEntities.skinnedMesh.bones[0].name + "\n" + CacheEntities.skinnedMesh.bones[1].name + "\n" + CacheEntities.skinnedMesh.bones[2].name
                + "\n" + CacheEntities.skinnedMesh.bones[3].name + "\n" + CacheEntities.skinnedMesh.bones[4].name + "\n" + CacheEntities.skinnedMesh.bones[5].name + "\n" + CacheEntities.skinnedMesh.bones[6].name
                + "\n" + CacheEntities.skinnedMesh.bones[7].name + "\n" + CacheEntities.skinnedMesh.bones[8].name + "\n" + CacheEntities.skinnedMesh.bones[9].name + "\n" + CacheEntities.skinnedMesh.bones[10].name
                + "\n" + CacheEntities.skinnedMesh.bones[11].name + "\n" + CacheEntities.skinnedMesh.bones[12].name + "\n" + CacheEntities.skinnedMesh.bones[13].name + "\n" + CacheEntities.skinnedMesh.bones[14].name
                + "\n" + CacheEntities.skinnedMesh.bones[15].name + "\n" + CacheEntities.skinnedMesh.bones[15].name*/ /*+ "\n" + CacheEntities.skinnedMesh.bones[16].name + "\n" + CacheEntities.skinnedMesh.bones[90].name*/
            /*+ "\n" + CacheEntities.skinnedMesh.bones[18].name + "\n" + CacheEntities.skinnedMesh.bones[19].name + "\n" + CacheEntities.skinnedMesh.bones[20].name + "\n" + CacheEntities.skinnedMesh.bones[21].name
            *///);

            if (drawMenu)
            {
                mainWRect = GUILayout.Window(mainWID, mainWRect, cheatmenu, "");
            }

            //hitcheck();

        }
        private void cheatmenu(int id)
        {

            GUI.Box(new Rect(1f, 1f, (mainWRect).width - 2f, 25f), "", GUI.skin.window);
            GUI.Box(new Rect(1f, 26f, 76f, (mainWRect).height - 27f), "", GUI.skin.window);

            if (GUI.Button(new Rect(1f, 31f, 75f, 25f), "esp")) mainWID = 0;
            if (GUI.Button(new Rect(1f, 61f, 75f, 25f), "aimbot")) mainWID = 1;
            if (GUI.Button(new Rect(1f, 91f, 75f, 25f), "misc")) mainWID = 2;

            if (id == 0)
            {
                GUILayout.BeginVertical();
                {
                    GUI.Label(new Rect((float)(mainWRect.width / 2.0 - 15.0), 1f, 250f, 100f), "esp settings");
                    GUI.Box(new Rect(1f, 31f, 75f, 25f), "");
                    GUI.Box(new Rect(76f, 26f, (mainWRect).width - 77f, 69f), "");

                    if (GUI.Button(new Rect(80f, 30f, 16f, 16f), "<"))
                    {
                        esp.D_box = true;
                        esp.box3d = false;
                    }
                    if (esp.D_box)
                    {
                        GUI.Label(new Rect(100f, 28f, 60f, 20f), "2D box");
                    }
                    else if (esp.box3d)
                    {
                        GUI.Label(new Rect(100f, 28f, 60f, 20f), "3D box");
                    }
                    if (GUI.Button(new Rect(145f, 30f, 16f, 16f), ">"))
                    {
                        esp.box3d = true;
                        esp.D_box = false;
                    }
                    esp.player_info = GUI.Toggle(new Rect(78f, 45f, 90f, 15f), esp.player_info,
                         "player info");
                    esp.chams = GUI.Toggle(new Rect(78f, 65f, 90f, 15f),
                        esp.chams, "chams");
                    esp._skeleton = GUI.Toggle(new Rect(170f, 30f, 90f, 20f), esp._skeleton, "skeleton");
                    esp.transport_esp = GUI.Toggle(new Rect(170f, 45f, 125f, 15f),
                       esp.transport_esp, "transport esp");
                }
                GUILayout.EndVertical();
            }
            if (id == 1)
            {
                GUILayout.BeginVertical();
                {
                    GUI.Label(new Rect((float)(mainWRect.width / 2.0 - 15.0), 1f, 250f, 100f), "A / W settings");
                    GUI.Box(new Rect(1f, 31f, 75f, 25f), "");
                    GUI.Box(new Rect(76f, 26f, (mainWRect).width - 77f, 104f), "");
                   // aimbot._silent_aim = GUI.Toggle(new Rect(78f, 30f, 90f, 15f), aimbot._silent_aim, "silent aim");
                    //aimbot._silent_aim = GUITools.checkbox(new Vector2(78f, 30f), new Vector2(16, 16), "silent aim", aimbot._silent_aim);
                    aimbot._aimbot = GUI.Toggle(new Rect(78f, 45f, 90f, 15f), aimbot._aimbot, "aimbot");
                    aimbot.nearHelper = GUI.Toggle(new Rect(175f, 70f, 90f, 25f), aimbot.nearHelper, "near helper");
                    if (aimbot._aimbot)
                    {
                        GUI.Label(new Rect(78f, 60f, 90f, 25f), "aim fov");
                        aimbot.aimfov = GUI.HorizontalSlider(new Rect(78f, 75f, 90f, 25f), aimbot.aimfov, 20f, 360f);
                        GUI.Label(new Rect(78f, 90f, 90f, 25f), "aim distance");
                        aimbot.MaximumDistance = GUI.HorizontalSlider(new Rect(78f, 105f, 90f, 25f), aimbot.MaximumDistance, 20f, 360f);
                    }
                    GUI.Label(new Rect(170f, 30f, 90f, 25f), "friend list enter ID");
                    friend = GUI.TextArea(new Rect(170f, 45f, 90f, 25f), friend);

                    GUI.Box(new Rect(76f, 130f, (mainWRect).width - 77f, 69f), "");
                    GUI.Label(new Rect(78f, 127f, 250f, 100f), "");
                    _norecoil = GUI.Toggle(new Rect(78f, 135f, 90f, 15f), _norecoil, "no recoil");
                    fast_bullets = GUI.Toggle(new Rect(170f, 135f, 90f, 15f), fast_bullets, "fast bullets");
                    /*settings.weapon.fastreload = GUI.Toggle(new Rect(78f, 115f, 100f, 25f), settings.weapon.fastreload, "fast reload");
                    settings.weapon.always_fullauto =
                        GUI.Toggle(new Rect(78f, 135f, 120f, 25f), settings.weapon.always_fullauto, "always full auto");*/
                }
                GUILayout.EndVertical();
            }

            if (id == 2)
            {
                GUILayout.BeginVertical();
                {
                    GUI.Label(new Rect((float)(mainWRect.width / 2.0 - 15.0), 1f, 250f, 100f), "misc settings");
                    GUI.Box(new Rect(1f, 31f, 75f, 25f), "");
                    GUI.Box(new Rect(76f, 26f, (mainWRect).width - 77f, 69f), "");
                    fast_move =
                        GUI.Toggle(new Rect(78f, 30f, 90f, 15f), fast_move, "fast move");
                    flyhack = GUI.Toggle(new Rect(78f, 45f, 130f, 15f), flyhack, "noclip / wall exploit");
                    WantedBypass = GUI.Toggle(new Rect(78f, 65f, 100f, 15f), WantedBypass, "wanted bypass");
                    chatspammer = GUI.Toggle(new Rect(190f, 30f, 90f, 15f), chatspammer, "chat spam");
                    if (chatspammer)
                    {
                        textspam = GUI.TextArea(new Rect(220f, 50f, 110f, 25f), textspam);
                    }
                    GUI.Box(new Rect(76f, 95f, (mainWRect).width - 77f, 69f), "");
                    GUI.Label(new Rect(78f, 93f, 250f, 100f), "");
                    discord_link = GUI.TextArea(new Rect(78f, 100f, 100f, 25f), discord_link);
                    if (GUI.Button(new Rect(78f, 130f, 150f, 20f), "video panel exploit :)"))
                    {
                     //   CacheEntities.VideoPlayer.clEntity.ClVideoPlay(discord_link, 0);
                    }

                    /*if(GUI.Button(new Rect(78f, 145f, 150f, 20f), "hack house"))
                    {
                        CacheEntities.hackingMenu.GameOver(true);
                    }*/

                    customFov = GUI.Toggle(new Rect(190f, 100f, 90f, 15f), customFov, "field of view");
                    if (customFov)
                    {
                        CameraFov = (int)GUI.HorizontalSlider(new Rect(190f, 115f, 90f, 25f), CameraFov, 75, 120);
                    }
                    skyBox = GUI.Toggle(new Rect(300f, 30f, 105f, 20f), skyBox, "custom skybox");
                    if (skyBox)
                    {

                        if (GUI.Button(new Rect(315f, 50f, 16f, 16f), "<"))
                        {
                            s_rainbow = true;
                            s_night = false;
                        }
                        if (s_rainbow)
                        {
                            GUI.Label(new Rect(332f, 48f, 60f, 20f), "rainbow");
                        }
                        else if (s_night)
                        {
                            GUI.Label(new Rect(340f, 48f, 60f, 20f), "night");
                        }
                        else
                        {
                            GUI.Label(new Rect(340f, 48f, 60f, 20f), "none");
                        }
                        if (GUI.Button(new Rect(377f, 50f, 16f, 16f), ">"))
                        {
                            s_night = true;
                            s_rainbow = false;
                        }
                    }
                }
                GUILayout.EndVertical();
            }

            GUI.DragWindow(new Rect(0.0f, 2f, (mainWRect).width, 25f));
        }

        public void Start()
        {

        }

        protected void Update()
        {
            if (GetAsyncKeyState(0x2E))
            {
                Loader.unload();
            }
            
            if (GetAsyncKeyState(0x2D))
            {
                drawMenu = !drawMenu;
            }

            if (fast_move)
            {
                speedhack();
            }

            if (GetAsyncKeyState(0x4E))
            {

            }

            if (GetAsyncKeyState(0x42))
            {
                if (!flyhack)
                {
                    flyhack = true;
                }
                else
                {
                    flyhack = false;
                }
            }

            if (flyhack)
            {

                if (CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Forward))
                {
                    CacheEntities.localPlayer.transform.localPosition += 10f * Time.deltaTime * (CacheEntities.localPlayer.transform.forward * 2f);
                    CacheEntities.localPlayer.manager.clManager.myPlayer.input += 5f * Time.deltaTime * (CacheEntities.localPlayer.transform.forward * 2f);
                }
                if (CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Right))
                {
                    CacheEntities.localPlayer.transform.localPosition += 10f * Time.deltaTime * (CacheEntities.localPlayer.transform.right * 2f);
                    CacheEntities.localPlayer.manager.clManager.myPlayer.input += 5f * Time.deltaTime * (CacheEntities.localPlayer.transform.right * 2f);
                }
                if (CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Left))
                {
                    CacheEntities.localPlayer.transform.localPosition += 10f * Time.deltaTime * (-CacheEntities.localPlayer.transform.right * 2f);
                    CacheEntities.localPlayer.manager.clManager.myPlayer.input += 5f * Time.deltaTime * (-CacheEntities.localPlayer.transform.right * 2f);
                }
                if (CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Backward))
                {
                    CacheEntities.localPlayer.transform.localPosition += 10f * Time.deltaTime * (-CacheEntities.localPlayer.transform.forward * 2f);
                    CacheEntities.localPlayer.manager.clManager.myPlayer.input += 5f * Time.deltaTime * (-CacheEntities.localPlayer.transform.forward * 2f);
                }
                if (CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Jump))
                {
                    CacheEntities.localPlayer.transform.localPosition += 10f * Time.deltaTime * (CacheEntities.localPlayer.transform.up * 2f);
                    CacheEntities.localPlayer.manager.clManager.myPlayer.input += 5f * Time.deltaTime * (CacheEntities.localPlayer.transform.up * 2f);
                }
                if (CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Crouch))
                {
                    CacheEntities.localPlayer.transform.localPosition += 10f * Time.deltaTime * (-CacheEntities.localPlayer.transform.up * 2f);
                    CacheEntities.localPlayer.manager.clManager.myPlayer.input += 5f * Time.deltaTime * (-CacheEntities.localPlayer.transform.up * 2f);
                }
            }



            if (_norecoil)
            {
                norecoil();
                //checkhit();
            }

            if (chatspammer)
            {
                CacheEntities.localPlayer.manager.clManager.SendToServer(PacketFlags.Reliable, SvPacket.GlobalMessage,
                    new object[]
                    {
                        textspam
                    });
            }

            CacheEntities.localPlayer.manager.clManager.SendToServer(PacketFlags.Reliable, SvPacket.Collect,
                    new object[]
                    {
                        CacheEntities.localPlayer.transform.position
                    });

            CacheEntities.localPlayer.manager.clManager.SendToServer(PacketFlags.Reliable, SvPacket.Heal, new object[]
            {
                    CacheEntities.localPlayer.ID
            });

            CacheEntities.localPlayer.manager.clManager.SendToServer(PacketFlags.Reliable, SvPacket.GodMode,
                Array.Empty<object>());
            CacheEntities.localPlayer.manager.clManager.HasPermissionClient(BrokeProtocol.Utility.PermEnum.Kick);
            CacheEntities.localPlayer.manager.clManager.HasPermissionClient(BrokeProtocol.Utility.PermEnum.Ban);
            CacheEntities.localPlayer.manager.clManager.HasPermissionClient(BrokeProtocol.Utility.PermEnum.UnbanIP);
            CacheEntities.localPlayer.manager.clManager.HasPermissionClient(BrokeProtocol.Utility.PermEnum.GodMode);
            CacheEntities.localPlayer.manager.clManager.HasPermissionClient(BrokeProtocol.Utility.PermEnum.Teleport);
           // CacheEntities.localPlayer.manager.clManager.HasPermissionClient(BrokeProtocol.Utility.PermEnum.cle);
            CacheEntities.localPlayer.manager.clManager.HasPermissionClient(BrokeProtocol.Utility.PermEnum.Heal);

            fieldFov();
            sk_changer();

            if (!skyBox)
            {
                s_rainbow = false;
                s_night = false;
            }

            if (WantedBypass)
            {
               // CacheEntities.localPlayer.svPlayer.SvClearCrimes();
                CacheEntities.localPlayer.svPlayer.SvClearInjuries();

            }

            if (fast_bullets)
            {
                CacheEntities.shBallistic.muzzleVelocity = 1200f;
                anti_aim();
            }



            CacheEntities.shHitscan.range = 1000f;

            CacheEntities.localPlayer.CanSeeEntity(CacheEntities.VideoPlayer, false, 999999f);
            //damage_hack();

        }

        private IEnumerator OpenInventoryDelay(ShPlayer player, int entityID, float delay, bool force = false)
        {
            yield return new WaitForSeconds(delay);
            player.svPlayer.SvView(entityID, force);
        }

        void checkhit()
        {
            var locoplayer = CacheEntities.localPlayer;

            var target = CacheEntities.nearestTarget;

            if (target == null)
               return;

          
        }

        protected void sk_changer()
        {
            if (skyBox)
            {
                if (CacheEntities.localPlayer.InApartment)
                    return;

                if (s_rainbow)
                {
                    CacheEntities.sceneManager.curSkyColor = new Color(esp.r, esp.g, esp.b, 1);
                    CacheEntities.sceneManager.curCloudColor = Color.white;
                }
                else if (s_night)
                {
                    CacheEntities.sceneManager.curSkyColor = new Color(0, 0, 0, 0.7f);
                    CacheEntities.sceneManager.curCloudColor = Color.white;
                }
            }
        }

        protected void fieldFov()
        {
            if (customFov)
            {
                MainCamera.Instance.defaultFOV = CameraFov;
            }
            else
            {
                MainCamera.Instance.defaultFOV = 75;
            }
        }


        protected void hitcheck()
        {
            RaycastHit raycastHit = default;
           
            if (CacheEntities.shHitscan.checkHitscan)
            {
                //GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 100f, 60f), "PLAYER HITED!!!");
              //  System.Media.SoundPlayer player = new System.Media.SoundPlayer(@"c:\hit.wav");
               // player.Play();
            }
        }

        protected void speedhack()
        {
            CacheEntities.localPlayer.SetStance(StanceIndex.Stand); //speed hack more forgivnes
            

            CacheEntities.localPlayer.isUnderwater = true;
            CacheEntities.localPlayer.animator.SetBool(Animations.swimming, CacheEntities.localPlayer.isUnderwater);

            CacheEntities.localPlayer.speedLimit = 15f;
            CacheEntities.localPlayer.maxSpeed = 30f;

            if (CacheEntities.localPlayer.IsDriving)
            {
                CacheEntities.localPlayer.GetControlled.speedLimit = 1000f;
                CacheEntities.localPlayer.GetControlled.maxSpeed = 2200f;
            }
        }

        protected void anti_aim()
        {
        }

        protected void damage_hack()
        {
            CacheEntities.destroyable.svDestroyable.Damage(CacheEntities.shBallistic.DamageProperty, CacheEntities.shBallistic.damage = 999f, CacheEntities.shBallistic.controller, null, default, default);

        }

        protected void norecoil()
        {
           
            CacheEntities.get_activeWeapon.recoilFactor = 0f;
            CacheEntities.get_activeWeapon.curSight.recoilBuff = 0f;
            CacheEntities.get_activeWeapon.curMuzzle.recoilBuff = 0f;
            CacheEntities.get_activeWeapon.curUnderbarrel.recoilBuff = 0f;

            CacheEntities.get_activeWeapon.accuracyFactor = 0f;

        }

    }
}
