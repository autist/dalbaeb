﻿
using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace brokeprotocolHACK
{
    //Class made by donrevan
    //UC.ME/forum/c-/213492-hook-managed-function.html
    public class HookManager
    {
        private const uint HOOK_SIZE_X64 = 12;
        private const uint HOOK_SIZE_X86 = 7;
        private byte[] original = null;

        public MethodInfo OriginalMethod = null;
        public MethodInfo HookMethod = null;

        public HookManager()
        {
            original = null;
            OriginalMethod = HookMethod = null;
        }

        public HookManager(MethodInfo orig, MethodInfo hook)
        {
            original = null;
            Init(orig, hook);
        }

        public void Init(MethodInfo orig, MethodInfo hook)
        {
            orig.MethodHandle.GetFunctionPointer();
            hook.MethodHandle.GetFunctionPointer();

            //RuntimeHelpers.PrepareMethod(orig.MethodHandle);
            //RuntimeHelpers.PrepareMethod(hook.MethodHandle);

            OriginalMethod = orig;
            HookMethod = hook;

        }
        public void Hook()
        {
            // Check if function is already hooked
            if (original != null)
                return;

            // Patch it
            IntPtr funcFrom = OriginalMethod.MethodHandle.GetFunctionPointer();
            IntPtr funcTo = HookMethod.MethodHandle.GetFunctionPointer();
            uint oldProt;
            if (IntPtr.Size == 8) //x86-64
            {
                original = new byte[HOOK_SIZE_X64];

                Import.VirtualProtect(funcFrom, (IntPtr)HOOK_SIZE_X64, 0x40, out oldProt);
                unsafe
                {
                    byte* ptr = (byte*)funcFrom;

                    for (int i = 0; i < HOOK_SIZE_X64; ++i)
                    {
                        original[i] = ptr[i];
                    }

                    // movabs rax, addy
                    // jmp rax
                    *(ptr) = 0x48;
                    *(ptr + 1) = 0xb8;
                    *(IntPtr*)(ptr + 2) = funcTo;
                    *(ptr + 10) = 0xff;
                    *(ptr + 11) = 0xe0;
                }
                Import.VirtualProtect(funcFrom, (IntPtr)HOOK_SIZE_X64, oldProt, out oldProt);

            }
            else //assume x86
            {
                original = new byte[HOOK_SIZE_X86];

                Import.VirtualProtect(funcFrom, (IntPtr)HOOK_SIZE_X86, 0x40, out oldProt);
                unsafe
                {
                    byte* ptr = (byte*)funcFrom;

                    for (int i = 0; i < HOOK_SIZE_X86; ++i)
                    {
                        original[i] = ptr[i];
                    }

                    // mov eax, addy
                    // jmp eax
                    *(ptr) = 0xb8;
                    *(IntPtr*)(ptr + 1) = funcTo;
                    *(ptr + 5) = 0xff;
                    *(ptr + 6) = 0xe0;
                }

                Import.VirtualProtect(funcFrom, (IntPtr)HOOK_SIZE_X86, oldProt, out oldProt);
            }
        }

        public void Unhook()
        {
            // Check if function is hooked
            if (original == null)
                return;

            // Restore original code
            uint oldProt;
            uint codeSize = (uint)original.Length;
            IntPtr origAddr = OriginalMethod.MethodHandle.GetFunctionPointer();
            Import.VirtualProtect(origAddr, (IntPtr)codeSize, 0x40, out oldProt);
            unsafe
            {
                byte* ptr = (byte*)origAddr;
                for (var i = 0; i < codeSize; ++i)
                {
                    ptr[i] = original[i];
                }
            }
            Import.VirtualProtect(origAddr, (IntPtr)codeSize, 0x40, out oldProt);

            // The original function has been restored.
            original = null;
        }

        internal class Import
        {
            [DllImport("kernel32.dll", SetLastError = true)]
            internal static extern bool VirtualProtect(IntPtr address, IntPtr size, uint newProtect, out uint oldProtect);
        }
    }
}
