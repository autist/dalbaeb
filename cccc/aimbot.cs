﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BrokeProtocol.API;
using BrokeProtocol.Entities;
using BrokeProtocol.Managers;
using BrokeProtocol.Required;
using BrokeProtocol.Utility;
using BrokeProtocol.Utility.Networking;
using ENet;
using Pathfinding;
using UnityEngine;
using BrokeProtocol.Client.UI;
using UnityEngine.InputSystem;
using System.Collections;

namespace brokeprotocolHACK
{
    class aimbot : MonoBehaviour
    {
        public static bool _aimbot = false;
        public static bool _silent_aim = false;
        public static bool nearHelper = false;


        public float x, y, z;

        public static float MaximumDistance { get; set; } = 120f;


        public static float aimfov { get; set; } = 75f;
        public bool aimming = false;
        public HitscanState state;

        public void Start()
        {

        }

        protected void Update()
        {

            CacheEntities.nearestTarget = get_nearest_player();


            if ((CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Fire) || CacheEntities.localPlayer.clPlayer.clManager.GetButton(InputType.Zoom)) && _aimbot)
            {
                if (CacheEntities.nearestTarget != null)
                {
                    var target = CacheEntities.nearestTarget.clPlayer.headBone.transform.position + new Vector3(0, 1, 0);
                    RotationPoint(target);

                }
            }


            if (nearHelper)
            {

                var dist = Vector3.Distance(CacheEntities.localPlayer.transform.position, CacheEntities.nearestTarget.transform.position);

                if (CacheEntities.nearestTarget != null && dist < 18f)
                {
                    CacheEntities.nearestTarget.transform.localPosition = CacheEntities.localPlayer.transform.position + CacheEntities.localPlayer.transform.forward * 5f;
                }
            }
            
            if (aimbot._silent_aim)
            {
                if (CacheEntities.nearestTarget)
                {

                    var target = CacheEntities.nearestTarget.clPlayer.headBone.transform.position + new Vector3(0, 1, 0);
                    var aim_angle = Prediction(target, CacheEntities.nearestTarget) - CacheEntities.shBallistic.controller.FutureOrigin;

                    var original_vel = CacheEntities.shHitscan.hitscanVelocity;
                    var aimbot_velocity = (aim_angle).normalized * Length(original_vel);

                    RotationPoint(target);
                    //  return aimbot_velocity;
                    //hooks.SAimbotHook.Hook();
                    //  CacheEntities.= Prediction(target, CacheEntities.nearestTarget) - CacheEntities.shBallistic.controller.FutureOrigin;
                    //Util.AimVector(CacheEntities.nearestTarget.GetOrigin + UnityEngine.Random.insideUnitSphere * 4f - CacheEntities.localPlayer.ActiveWeapon.GetWeaponPosition(), CacheEntities.localPlayer.Velocity - CacheEntities.nearestTarget.Velocity, CacheEntities.localPlayer.ActiveWeapon.WeaponVelocity, CacheEntities.localPlayer.ActiveWeapon.WeaponGravity, out target);
                }
                else
                {
                    //hooks.SAimbotHook.Unhook();
                }
            }
        }

        public float Length(Vector3 f)
        {
            return Mathf.Sqrt(f.x * f.x + f.y * f.y + f.z * f.z);
        }

        protected IEnumerator DamageMarker()
        {
            float time = Time.time;
            float endTime = time + 0.4f;
            GameObject damageMarker = Util.damageMarkerBuffer.Execute();
            QuadGraphic quad = damageMarker.GetComponent<QuadGraphic>();
            while (Time.time < endTime)
            {
                Vector3 direction = new Vector3(Mathf.Cos(CacheEntities.localPlayer.viewAngleLimit), 0f, Mathf.Sin(CacheEntities.localPlayer.viewAngleLimit));
                Vector3 vector = MonoBehaviourSingleton<MainCamera>.Instance.worldCameraT.InverseTransformDirection(direction);
                quad.rectTransform.rotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(vector.z, vector.x) * 57.29578f);
                quad.Initialize(new Vector2(300f, 0f), new Vector2(200f * (endTime - Time.time), 10f), new Color(1f, 0f, 0f, 0.4f));
                yield return null;
            }
            Util.damageMarkerBuffer.Release(damageMarker);
            yield break;
        }

        public Vector3 hk_FireVector()
        {
            var target = CacheEntities.nearestTarget.clPlayer.headBone.transform.position + new Vector3(0, 1, 0);
            Vector3 start = CacheEntities.shBallistic.controller.FutureOrigin;
            Vector3 end = Prediction(target, CacheEntities.nearestTarget);
            Vector3 dir = end - start;

            var original_vel = CacheEntities.shBallistic.WeaponVelocity;
            var aimbot_velocity = (dir).normalized * original_vel;

            return aimbot_velocity;

            hooks.SAimbotHook.OriginalMethod.Invoke(this, new object[] {  });

        }

        protected void RotationPoint(Vector3 target)
        {
            var a1 = (Prediction(target, CacheEntities.nearestTarget) - CacheEntities.shBallistic.controller.FutureOrigin).normalized;
            var rotation = Quaternion.LookRotation(a1);
            CacheEntities.localPlayer.svPlayer.player.SetRotation(rotation);
        }

        protected Vector3 Prediction(Vector3 target, ShPlayer player)
        {
            var num = Vector3.Distance(CacheEntities.localPlayer.transform.position, target);
            if (num > 0.001f)
            {
                var bullet_speed = CacheEntities.shBallistic.muzzleVelocity;
                var bullet_time = num / bullet_speed;
                var target_velocity = player.Velocity;
                var prediction_velocity = target_velocity * bullet_time * (bullet_speed / CacheEntities.shBallistic.range);
                target += prediction_velocity;
                var gravity = 0.04f * Physics.gravity.y;
                target.y += (4.905f * bullet_time * bullet_time) * gravity;
            }
            return target;
        }

        public Vector3 _OffsetToVector(Vector2 offset)
        {
            return CacheEntities.shHitscan.controller.originT.TransformDirection(Quaternion.Euler(offset.x, offset.y, 0f) * Vector3.forward);
        }

        public Vector3 OriginOffset(ShPlayer target)
        {
            var controller = CacheEntities.localPlayer.controller;
            Vector3 futureOrigin = controller.FutureOrigin;
            RaycastHit raycastHit;
            if (Physics.Raycast(futureOrigin, controller.originT.forward, out raycastHit, 2f, 26373))
            {
                return raycastHit.point;
            }
            return futureOrigin + controller.GetRotationT.forward * 2f;
        }

        

        public static ShPlayer get_nearest_player()
        {
            ShPlayer retrurn_ply = null;

            foreach (ShPlayer player in CacheEntities.players)
            {
               
                if (CacheEntities.worldCamera == null)
                {
                    continue;
                }

                if (CacheEntities.localPlayer == null)
                {
                    continue;
                }

                if (player != null && player.characterType == CharacterType.Humanoid && !player.IsDead &&
                    player != CacheEntities.localPlayer && player == player.isHuman && player.ID.ToString() != main.friend)
                {
                    var dist = Vector3.Distance(CacheEntities.localPlayer.transform.position, player.transform.position);
                    if (dist > MaximumDistance)
                        continue;

                    var headpoint = CacheEntities.worldCamera.WorldToScreenPoint(player.clPlayer.headBone.transform.position);
                    if (headpoint.z <= 0)
                        continue;

                    var screenpoint = Vector2.Distance(new Vector2(Screen.width / 2, Screen.height / 2),
                        new Vector2(headpoint.x, headpoint.y));
                    if (aimfov != null && screenpoint > aimfov)
                        continue;

                    if (retrurn_ply == null)
                    {
                        retrurn_ply = player;
                        continue;
                    }

                    var headpoint2 = CacheEntities.worldCamera.WorldToScreenPoint(retrurn_ply.clPlayer.headBone.transform.position);
                    var screenpoint2 = Vector2.Distance(new Vector2(Screen.width / 2, Screen.height / 2),
                        new Vector2(headpoint2.x, headpoint2.y));
                    if (aimfov != null && screenpoint2 > aimfov)
                        retrurn_ply = null;
                    if (screenpoint < screenpoint2)
                        retrurn_ply = player;


                }
            }

            return retrurn_ply;
        }
    }
}