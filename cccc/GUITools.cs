﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem;
using BrokeProtocol.Utility;
namespace brokeprotocolHACK
{
    static class GUITools
    {
        public static Texture2D white_texture = Texture2D.whiteTexture;

        public static void background(Rect pos, Color clr)
        {
            GUI.color = clr;
            GUI.DrawTexture(pos, white_texture);
        }

        public static void TextureImage()
        {
           
        }

        internal static bool tabs(Vector2 pos, Vector2 button_size, string name)
        {
            Event current = Event.current;
            switch (current.type)
            {
                case EventType.MouseDown:
                    {
                        if (new Rect(pos.x, pos.y, button_size.x, button_size.y).Contains(CacheEntities.unity_mousePosition))
                        {
                            return true;
                        }
                        break;
                    }


                case EventType.Repaint:
                    {
                        GUI.Label(new Rect(pos.x, pos.y - 5f, 100, 20), name);

                        if (true)
                        {
                            background(new Rect(pos.x, pos.y, button_size.x, button_size.y), new Color(0, 0, 1, 1));
                        }
                        else
                        {

                        }

                        break;
                    }

            }
            return false;
        }

        internal static bool checkbox(Vector2 pos, Vector2 button_size, string name, bool variable)
        {

            Event current = Event.current;
            switch (current.type)
            {
                case EventType.MouseDown:
                    {
                        if (new Rect(pos.x, pos.y, button_size.x, button_size.y).Contains(CacheEntities.unity_mousePosition))
                        {
                            variable = !variable;
                        }
                        break;
                    }


                case EventType.Repaint:
                    {
                        GUI.Label(new Rect(pos.x + 20f, pos.y - 2f, 100, 20), name);

                        if (variable)
                        {
                            background(new Rect(pos.x, pos.y, button_size.x, button_size.y), new Color(0, 1, 0, 1));
                        }
                        else
                        {
                            background(new Rect(pos.x, pos.y, button_size.x, button_size.y), new Color(0, 0, 0, 0.7f));
                        }
                        break;
                    }

            }

            return variable;

        }

        internal static void arrowVariable(Vector2 pos, Vector2 button_size, string name, string name2, bool variable, bool variable2)
        {
            if (GUI.Button(new Rect(pos.x, pos.y, button_size.x, button_size.y), "<"))
            {
                variable = true;
                variable2 = false;
            }
            if (GUI.Button(new Rect(pos.x + 65f, pos.y, button_size.x, button_size.y), ">"))
            {
                variable2 = true;
                variable = false;
            }

            if (variable)
            {
                GUI.Label(new Rect(pos.x + 20f, pos.y - 2f, 60f, 20f), name);
            }
            else if (variable2)
            {
                GUI.Label(new Rect(pos.x + 20f, pos.y - 2f, 60f, 20f), name2);
            }
        }
    }
}
